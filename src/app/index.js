'use strict';

angular.module('angularStore', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ui.router'])
  .config(function ($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
  })
  .controller('HeaderController', function($scope) {
    $scope.user = {
      name : 'John Doe'
    };
  })
;
