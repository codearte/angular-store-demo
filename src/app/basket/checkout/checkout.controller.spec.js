'use strict';

describe('CheckoutController', function () {
  var $rootScope, scope, BasketService, OrderService, deferredPlaceOrder;

  beforeEach(module('angularStore'));

  beforeEach(inject(function (_$rootScope_, $q) {
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();

    var orderLines = [{}];
    BasketService = {
      getOrderLines: sinon.stub().returns(orderLines),
      clearBasket: sinon.spy()
    };

    deferredPlaceOrder = $q.defer();
    OrderService = {
      placeOrder: sinon.stub().returns(deferredPlaceOrder.promise)
    };

  }));

  describe('when checkoutForm is valid', function () {
    var checkoutForm = {
      $valid: true
    };
    beforeEach(function () {
      scope.checkoutForm = checkoutForm;
    });

    it('should call OrderService to place an order', inject(function ($controller) {

      $controller('CheckoutController', {
        $scope: scope,
        OrderService: OrderService,
        BasketService: BasketService,
      });

      scope.placeOrder();

      expect(OrderService.placeOrder.called).toBeTruthy()

    }));

    it('should clear basket and show products after placing an order', inject(function ($controller) {

      var $state = {
        go: sinon.spy()
      };
      $controller('CheckoutController', {
        $scope: scope,
        OrderService: OrderService,
        BasketService: BasketService,
        $state : $state
      });

      scope.placeOrder();
      deferredPlaceOrder.resolve({});
      $rootScope.$digest();

      expect(BasketService.clearBasket.called).toBeTruthy()
      expect($state.go.calledWith('products')).toBeTruthy()
    }));
  });


  describe('when checkoutForm is NOT valid', function () {
    var checkoutForm = {
      $valid: false
    };
    beforeEach(function () {
      scope.checkoutForm = checkoutForm;
    });

    it('should not call OrderService', inject(function ($controller) {

      $controller('CheckoutController', {
        $scope: scope,
        OrderService: OrderService,
        BasketService: BasketService
      });

      scope.placeOrder();

      expect(OrderService.placeOrder.called).toBeFalsy()

    }));
  });
});
