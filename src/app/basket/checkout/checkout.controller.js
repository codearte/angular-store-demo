'use strict';

angular.module('angularStore')
  .config(function($stateProvider) {
    $stateProvider.state('basket.checkout', {
      url : '/basket/checkout',
      templateUrl : 'app/basket/checkout/checkout.html',
      controller : 'CheckoutController'
    });
  })
  .controller('CheckoutController', function($scope, OrderService, BasketService, $log, $window, $state) {
    $scope.checkout = {};
    $scope.placeOrder = function() {
      if ($scope.checkoutForm.$valid) {
        var orderLines = BasketService.getOrderLines();
        OrderService.placeOrder($scope.checkout, orderLines).then(function(order) {
          $log.debug('Placed order', order);
          BasketService.clearBasket();
          $state.go('products');
          $window.alert('Placed order: '+order.orderNumber);
        });
      }
    };
  });
