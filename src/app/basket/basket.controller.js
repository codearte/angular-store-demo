'use strict';

angular.module('angularStore')
  .config(function($stateProvider) {
    $stateProvider.state('basket', {
      url : '/basket',
      templateUrl : 'app/basket/basket.html',
      controller : 'BasketController'
    });
  })
  .controller('BasketController', function($scope, BasketService, $state) {
    $scope.orderLines = BasketService.getOrderLines();

    $scope.$watch('orderLines.length', function(orderLinesCount) {
      if (orderLinesCount === 0) {
        $state.go('products');
      }
    });

    $scope.incrementQuantity = function(orderLine) {
      BasketService.incrementQuantity(orderLine);
    };

    $scope.decrementQuantity = function(orderLine) {
      BasketService.decrementQuantity(orderLine);
    };

    $scope.getOrderValue = function() {
      return BasketService.getBasketValue();
    };
  });
