'use strict';

describe('BasketController', function () {
  var $rootScope, scope, BasketService;

  beforeEach(module('angularStore'));

  beforeEach(inject(function (_$rootScope_, _BasketService_) {
    $rootScope = _$rootScope_;
    BasketService = _BasketService_;
    scope = $rootScope.$new();
  }));

  describe('for empty orderLines', function () {

    it('should go to products page', inject(function ($controller) {
      spyOn(BasketService, 'getOrderLines').and.returnValue([]);

      var $state = {
        go: jasmine.createSpy('go')
      };

      $controller('BasketController', {
        $scope: scope,
        BasketService: BasketService,
        $state: $state
      });

      $rootScope.$digest();

      expect($state.go).toHaveBeenCalledWith('products');
    }));
  });
  //
  describe('for one product in orderLines', function () {
    var orderLines = [{'mocked': 'orderLine'}];

    it('should call BasketService for incrementing quantity', inject(function ($controller, BasketService) {
      spyOn(BasketService, 'getOrderLines').and.returnValue(orderLines);
      spyOn(BasketService, 'incrementQuantity');

      $controller('BasketController', {
        $scope: scope,
        BasketService: BasketService
      });

      scope.incrementQuantity(orderLines[0]);

      expect(BasketService.incrementQuantity).toHaveBeenCalledWith(orderLines[0]);
    }));

    it('should call BasketService for decrementing quantity', inject(function ($controller, BasketService) {
      spyOn(BasketService, 'getOrderLines').and.returnValue(orderLines);
      spyOn(BasketService, 'decrementQuantity');

      $controller('BasketController', {
        $scope: scope,
        BasketService: BasketService,
        $state: {
          go: function () {
          }
        }
      });

      scope.$digest();
      scope.decrementQuantity(orderLines[0]);

      expect(BasketService.decrementQuantity).toHaveBeenCalledWith(orderLines[0]);
    }));
    it('should call BasketService for getting order value', inject(function ($controller, BasketService) {
      spyOn(BasketService, 'getBasketValue').and.returnValue(10);

      $controller('BasketController', {
        $scope: scope,
        BasketService: BasketService
      });

      scope.getOrderValue(orderLines[0]);

      expect(BasketService.getBasketValue).toHaveBeenCalled();
    }));
  });

  /*  it('should fetch products during resolve', inject(function($state, $templateCache) {
   ProductService.fetchProducts = jasmine.createSpy('fetchProducts');
   $templateCache.put('app/products/products.html', '');

   $state.go('products');
   $rootScope.$digest();

   expect(ProductService.fetchProducts).toHaveBeenCalled();
   }));

   it('should set products on $scope', inject(function ($controller) {

   expect(scope.products).toBeUndefined();

   $controller('ProductsController', {
   $scope: scope,
   products : products
   });

   expect(angular.isArray(scope.products)).toBeTruthy();
   expect(scope.products.length === products.length).toBeTruthy();
   }));

   it('should add product to basket', inject(function ($controller, $state, BasketService) {

   spyOn(BasketService, 'addToBasket');

   expect(scope.products).toBeUndefined();

   $controller('ProductsController', {
   $scope: scope,
   products : products,
   BasketService : BasketService
   });

   var event = {
   stopPropagation : sinon.spy()
   };

   scope.buyItem(scope.products[0], event);

   expect(BasketService.addToBasket).toHaveBeenCalled();
   expect(event.stopPropagation.called).toBeTruthy();
   }));*/
});
