'use strict';

describe('ProductsController', function () {
  var $rootScope, scope, ProductService = {},
    products = [{
    id: 1,
    title: 'A Smarter Way to Learn JavaScript: The new approach that uses technology to cut your effort in half',
    thumbnailUrl: 'http://ecx.images-amazon.com/images/I/51fuxWUUIpL._AA160_.jpg',
    price: 17.96,
    author: 'Mark Myers',
    rating: 4.8
  }];

  beforeEach(module('angularStore', function($provide) {
    $provide.value('ProductService', ProductService);
  }));

  beforeEach(inject(function (_$rootScope_) {
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
  }));

  it('should fetch products during resolve', inject(function($state, $templateCache) {
    ProductService.fetchProducts = jasmine.createSpy('fetchProducts');
    $templateCache.put('app/products/products.html', '');

    $state.go('products');
    $rootScope.$digest();

    expect(ProductService.fetchProducts).toHaveBeenCalled();
  }));

  it('should set products on $scope', inject(function ($controller) {

    expect(scope.products).toBeUndefined();

    $controller('ProductsController', {
      $scope: scope,
      products : products
    });

    expect(angular.isArray(scope.products)).toBeTruthy();
    expect(scope.products.length === products.length).toBeTruthy();
  }));

  it('should add product to basket', inject(function ($controller, $state, BasketService) {

    spyOn(BasketService, 'addToBasket');

    expect(scope.products).toBeUndefined();

    $controller('ProductsController', {
      $scope: scope,
      products : products,
      BasketService : BasketService
    });

    var event = {
      stopPropagation : sinon.spy()
    };

    scope.buyItem(scope.products[0], event);

    expect(BasketService.addToBasket).toHaveBeenCalled();
    expect(event.stopPropagation.called).toBeTruthy();
  }));
});
