'use strict';

angular.module('angularStore')
  .config(function ($stateProvider) {
    $stateProvider.
      state('productDetails', {
        url: '/products/:productId',
        templateUrl: 'app/products/product-details/product-details.html',
        controller : 'ProductDetailsController',
        resolve : {
          product : function(ProductService, $stateParams) {
            return ProductService.fetchProduct($stateParams.productId);
          }
        }
      });
  })
  .controller('ProductDetailsController', function($scope, product, BasketService) {
    $scope.product = product;

    $scope.addToBasket = function() {
      BasketService.addToBasket(product);
    };
  });
