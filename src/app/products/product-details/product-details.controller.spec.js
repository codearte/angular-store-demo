'use strict';

describe('ProductDetailsController', function () {
  var $rootScope, scope, BasketService, OrderService, deferredPlaceOrder;

  beforeEach(module('angularStore'));

  beforeEach(inject(function (_$rootScope_, $q) {
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();

    BasketService = {
      addToBasket: sinon.spy()
    };

  }));

  it('should call BasketService to add product to basket', inject(function($controller) {
    var product = {};

    $controller('ProductDetailsController', {
      $scope : scope,
      BasketService : BasketService,
      product : product
    });

    $rootScope.$digest();
    scope.addToBasket();

    expect(BasketService.addToBasket.calledWith(product)).toBeTruthy();
  }));

});
