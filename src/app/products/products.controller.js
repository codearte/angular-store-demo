'use strict';

angular.module('angularStore')
  .config(function ($stateProvider) {
    $stateProvider
      .state('products', {
        url: '/',
        templateUrl: 'app/products/products.html',
        controller: 'ProductsController',
        resolve : {
          'products' : function(ProductService) {
            return ProductService.fetchProducts();
          }
        }
      });
  })
  .controller('ProductsController', function ($scope, products, $state, BasketService) {
    $scope.products = products;

    $scope.buyItem = function(product, $event) {
      $event.stopPropagation();
      BasketService.addToBasket(product);
    };

    $scope.view = function(product) {
      $state.go('productDetails', { productId : product.id });
    };


  });
