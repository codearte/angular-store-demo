'use strict';

angular.module('angularStore')
.service('BasketService', function() {

    function OrderLine(product) {
      this.product = product;
      this.quantity = 1;
    }
    OrderLine.prototype.getTotalValue = function() {
      return this.quantity * this.product.price;
    };

    var orderLines = [];

    var findOrderLineByProduct = function(product) {
      return _.find(orderLines, function(orderLine) {
        return orderLine.product.id === product.id;
      });
    };

    this.addToBasket = function(product) {
      var orderLine = findOrderLineByProduct(product);
      if (orderLine) {
        orderLine.quantity++;
      } else {
        orderLines.push(new OrderLine(product));
      }
    };

    this.incrementQuantity = function(orderLine) {
      orderLine.quantity++;
    };

    this.decrementQuantity = function(orderLine) {
      if (orderLine.quantity === 1) {
        _.remove(orderLines, function(item) {
          return item === orderLine;
        });
      } else {
        orderLine.quantity--;
      }
    };

    this.getBasketValue = function() {
      return _.reduce(orderLines, function(result, orderLine) {
        return result + orderLine.getTotalValue();
      }, 0);
    };

    this.getOrderLines = function() {
      return orderLines;
    };

    this.clearBasket = function() {
      orderLines.length = 0;
    };

  });
