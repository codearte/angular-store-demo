'use strict';

angular.module('angularStore')
  .directive('basket', function () {

    return {
      restrict : 'E',
      templateUrl : 'components/basket/basket.directive.html',
      controller : function($scope, BasketService) {
        var orderLines = BasketService.getOrderLines();


        $scope.getItemsCount = function() {
          var itemsCount = _.reduce(orderLines, function(result, orderLine) {
            return result + orderLine.quantity;
          }, 0);
          return itemsCount;
        };

        $scope.getTotalValue = function() {
          return BasketService.getBasketValue();
        };
      }
    };
  });
