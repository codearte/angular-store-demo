'use strict';

angular.module('angularStore')
  .service('ProductService', function ($http) {
    this.fetchProducts = function() {
      return $http.get('/api/products').then(function(response) {
        return response.data;
      });
    };
    this.fetchProduct = function(id) {
      return $http.get('/api/products/'+id).then(function(response) {
        return response.data;
      });
    };
  });
