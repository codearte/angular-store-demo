'use strict';

describe('ProductService', function() {
  var $rootScope, $httpBackend, ProductService, products = [{
    id: 1,
    title: 'A Smarter Way to Learn JavaScript: The new approach that uses technology to cut your effort in half',
    thumbnailUrl: 'http://ecx.images-amazon.com/images/I/51fuxWUUIpL._AA160_.jpg',
    price: 17.96,
    author: 'Mark Myers',
    rating: 4.8
  }];

  beforeEach(module('angularStore'));

  beforeEach(inject(function(_$rootScope_, _$httpBackend_, _ProductService_){
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    ProductService = _ProductService_;

    $httpBackend.whenGET('/api/products').respond(products);
    $httpBackend.whenGET('/api/products/1').respond(products[0]);
  }));

  it('should return products list', function() {
    var productsHandler = sinon.spy();
    var productsPromise = ProductService.fetchProducts();

    $httpBackend.expectGET('/api/products');
    $httpBackend.flush();

    productsPromise.then(productsHandler);
    $rootScope.$digest();

    expect(productsHandler.calledWith(products)).toBeTruthy();
  });

  it('should return product for id', function() {
    var productHandler = sinon.spy();
    var productsPromise = ProductService.fetchProduct(1);

    $httpBackend.expectGET('/api/products/1');
    $httpBackend.flush();

    productsPromise.then(productHandler);
    $rootScope.$digest();

    expect(productHandler.calledWith(products[0])).toBeTruthy();
  });
});
