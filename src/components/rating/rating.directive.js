'use strict';

angular.module('angularStore')
  .directive('rating', function() {
    return {
      restrict : 'A',
      scope : {
        rating : '='
      },
      link : function(scope, el) {
        var rating = scope.rating || 0;
        for (var i=1; i<=5; i++) {
          if (i <= rating) {
            angular.element(el).append('<span class="glyphicon glyphicon-star"></span>');
          } else {
            angular.element(el).append('<span class="glyphicon glyphicon-star-empty"></span>');
          }

        }
      }
    };
  });
