'use strict';

describe('Rating directive', function() {
  var $compile,
      $rootScope;

  beforeEach(module('angularStore'));

  // Store references to $rootScope and $compile
  // so they are available to all tests in this describe block
  beforeEach(inject(function(_$compile_, _$rootScope_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  it('shows five empty stars for 0 rating', function() {
    var element = $compile('<div rating="0"></div>')($rootScope);

    $rootScope.$digest();

    var starSpans = element.children();
    expect(starSpans.length).toBe(5);
    for (var i = 0; i < starSpans.length; i++) {
      var starSpan = angular.element(starSpans[i]);
      expect(starSpan.hasClass('glyphicon-star-empty')).toBeTruthy();
    }
  });

  it('shows three filled stars for 3 rating', function() {
    var element = $compile('<div rating="3"></div>')($rootScope);

    $rootScope.$digest();

    var starSpans = element.children();
    expect(starSpans.length).toBe(5);
    for (var i = 0; i < 3; i++) {
      var starSpan = angular.element(starSpans[i]);
      expect(starSpan.hasClass('glyphicon-star')).toBeTruthy();
    }
  });
});
