'use strict';

angular.module('angularStore')
  .filter('price', function() {
    return function(value) {
      if (!_.isNumber(value)) {
        value = parseFloat(value);
      }
      return '$'+value.toFixed(2);
    };
  });
