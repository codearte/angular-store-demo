'use strict';

describe('OrderService', function () {
  var $rootScope, $httpBackend, OrderService,
    customerData = {
      name: 'John Smith'
    },
    orderLines = [
      {'some': 'product'}
    ],
    orderResponse = {
      orderNumber: 1
    };

  beforeEach(module('angularStore'));

  beforeEach(inject(function (_$rootScope_, _$httpBackend_, _OrderService_) {
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    OrderService = _OrderService_;

    $httpBackend.whenPOST('/api/orders/', {
      customer: customerData,
      orderLines: orderLines
    }).respond(orderResponse);
  }));

  it('should placeOrder using POST', function () {
    var orderHandler = sinon.spy();
    var orderPromise = OrderService.placeOrder(customerData, orderLines);

    $httpBackend.expectPOST('/api/orders/', {
      customer : customerData,
      orderLines : orderLines
    });
    $httpBackend.flush();

    orderPromise.then(orderHandler);
    $rootScope.$digest();

    expect(orderHandler.calledWith(orderResponse)).toBeTruthy();
  });

});
