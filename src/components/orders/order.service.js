'use strict';

angular.module('angularStore').
  service('OrderService', function($http) {
    this.placeOrder = function(customerData, orderLines) {
      return $http.post('/api/orders/', {
        customer : customerData,
        orderLines : orderLines
      }).then(function(response) {
        return response.data;
      });
    };
  });
