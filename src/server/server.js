'use strict';

// simple express server
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();
var orderNumber = 12345;
var products = [
      {
        id : 1,
        title: 'A Smarter Way to Learn JavaScript: The new approach that uses technology to cut your effort in half',
        thumbnailUrl: 'http://ecx.images-amazon.com/images/I/51fuxWUUIpL._AA160_.jpg',
        price: 17.96,
        author: 'Mark Myers',
        rating: 4.8
      },
      {
        id : 2,
        title: 'JavaScript  The Good Parts',
        thumbnailUrl: 'http://ecx.images-amazon.com/images/I/518QVtPWA7L._AA160_.jpg',
        price: 17.39,
        author: 'Douglas Crockford',
        rating: 4.5
      },
      {
        id : 3,
        title: 'Secrets of the JavaScript Ninja',
        thumbnailUrl: 'http://ecx.images-amazon.com/images/I/51UYwOhvQPL._AA160_.jpg',
        price: 28.51,
        author: 'John Resig and Bear Bibeault',
        rating: 4.1
      },
      {
        id : 4,
        title: 'Pro AngularJS (Expert\'s Voice in Web Development)',
        thumbnailUrl: 'http://ecx.images-amazon.com/images/I/51mb%2BndLOvL._AA160_.jpg',
        price: 25.64,
        author: 'Adam Freeman',
        rating: 4.3
      },
      {
        id : 5,
        title: 'Recipes with Angular.js',
        thumbnailUrl: 'http://ecx.images-amazon.com/images/I/41GQQvK-FwL._AA160_.jpg',
        price: 12.29,
        author: 'Frederik Dietz',
        rating: 4.4
      },
      {
        id : 6,
        title: 'AngularJS: Novice to Ninja',
        thumbnailUrl: 'http://ecx.images-amazon.com/images/I/41tVsGAB0cL._AA160_.jpg',
        price: 27.59,
        author: 'Sandeep Panda',
        rating: 2.0
      }
    ];

app.use(bodyParser.json());

app.get('/api/products/', function(req, res) {
    res.json(products);
});

app.get('/api/products/:id', function(req, res) {
	var idx = (req.params.id - 1);
  if (idx >= 0 && idx < products.length) {
    res.json(products[idx]);
  } else {
    res.sendStatus(404);
  }
});
app.post('/api/orders/', function(req, res) {
  var order = req.body;
  order.orderNumber = orderNumber++;
  res.json(order);
});

app.listen(3030);
