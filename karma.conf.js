'use strict';

module.exports = function (config) {

  config.set({
    autoWatch: false,

    frameworks: ['jasmine', 'sinon'],

    browsers: ['PhantomJS'],

    reporters: ['progress', 'coverage'],

    preprocessors: {
      // source files, that you wanna generate coverage for
      // do not include tests or libraries
      // (these files will be instrumented by Istanbul)
      'src/**/!(*spec|*mock).js' : ['coverage']
    },

    // optionally, configure the reporter
    coverageReporter: {
      type : 'html',
      dir : 'coverage/'
    },

    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-sinon',
      'karma-coverage'
    ]
  });
};
